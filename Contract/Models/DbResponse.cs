﻿using System;

namespace Database.Interface.Contract.Models
{
    public class DbResponse
    {
        public DbResponse(bool isSuccess, Exception ex = null, object returnValue = null)
        {
            IsSuccess = isSuccess;
            Exception = ex;
            ReturnValue = returnValue;
        }

        public bool IsSuccess { get; }

        public Exception Exception { get; }

        public string Message => Exception?.Message;

        public object ReturnValue { get; }
    }
}
