﻿using System.Collections.Generic;
using Database.Interface.Contract.Models;

namespace Database.Interface.Contract
{
    public interface IDbObject<T> where T : class
    {
        /// <summary>
        /// Fetches all records and returns the highest itemKey (if no records are found - returns -1)
        /// </summary>
        /// <returns></returns>
        int GetHighestItemKey();

        /// <summary>
        /// Fetch all text as string
        /// </summary>
        string GetRawData();

        /// <summary>
        /// Fetch all text and convert it to collection of current type
        /// </summary>
        IEnumerable<T> GetRecords();

        /// <summary>
        /// Fetches all records and returns the one with matching key (if no matches was found - returns null)
        /// </summary>
        /// <param name="index">Item's key</param>
        /// <returns></returns>
        T GetRecord(int dbItemKey);

        /// <summary>
        /// Insert/Add new record (if local file does not exist - it will be created)
        /// </summary>
        /// <param name="record">Record to insert/add</param>
        /// <returns></returns>
        DbResponse InsertRecord(T record);

        /// <summary>
        /// Gets highest existing item key, increment it with one, and inserts new record (if local file does not exist - it will be created)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        DbResponse InsertWithNextHighestItemKey(T item);

        /// <summary>
        /// Insert/Add new records (if local file does not exist - it will be created)
        /// </summary>
        /// <param name="records">Records to insert/add</param>
        /// <returns></returns>
        DbResponse InsertRecords(IEnumerable<T> records);

        /// <summary>
        /// Modify/Update selected records (if selected does not exist - throws exception)
        /// </summary>
        /// <param name="dbItemKey">ID</param>
        /// <param name="record">Object to replace the old one</param>
        /// <returns></returns>
        DbResponse ModifyRecord(int dbItemKey, T record);

        /// <summary>
        /// Delete/Remove selected record (if selected does not exits - throws exception)
        /// </summary>
        /// <param name="dbItemKey">Item Key</param>
        /// <returns></returns>
        DbResponse DeleteRecord(int dbItemKey);

        /// <summary>
        /// Delete/Remove selected records (if selected does not exits - throws exception)
        /// </summary>
        /// <param name="dbItemKeys">Item Keys</param>
        /// <returns></returns>
        DbResponse DeleteRecords(IEnumerable<int> dbItemKeys);
    }
}
