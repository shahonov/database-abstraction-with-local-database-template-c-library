﻿using System;

namespace Database.Interface.Implementations.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class LocalDbItem : Attribute
    {
        public LocalDbItem(string path)
        {
            Path = path;
        }

        public string Path { get; }
    }
}
