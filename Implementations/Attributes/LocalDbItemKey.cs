﻿using System;

namespace Database.Interface.Implementations.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class LocalDbItemKey : Attribute { }
}
