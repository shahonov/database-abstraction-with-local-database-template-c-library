﻿using System;
using System.IO;
using System.Text;
using System.Linq;
using Newtonsoft.Json;
using System.Reflection;
using System.Collections.Generic;
using Database.Interface.Contract;
using Database.Interface.Contract.Models;
using Database.Interface.Implementations.Attributes;

namespace Database.Interface.Implementations
{
    public class FileObject<T> : IDbObject<T> 
        where T : class, ICloneable
    {
        private string _filePath;

        private string _itemKeyPropName;

        private string _dBPath;

        /// <summary>
        /// Creates instance that allows you to perform local database operations over .txt files
        /// which location is at the main solution folder + /Database...
        /// </summary>
        /// <param name="dbPath">Specifies sub-folder of /Database, default value is null</param>
        public FileObject(string dbPath = null)
        {
            var attribute = typeof(T).GetCustomAttribute(typeof(LocalDbItem));
            var dir = $"../../..//Database/{dbPath}";
            _filePath = $"{((LocalDbItem)attribute).Path}.txt";
            _dBPath += $"{dir}/{_filePath}";

            SetItemKeyPropName();
            SetDatabaseDirectories(dir);
        }

        public int GetHighestItemKey()
        {
            var itemKeyValue = -1;
            var records = GetRecords();
            if (records == null)
            {
                return -1;
            }

            foreach (var record in records)
            {
                var value = GetItemKeyValue(record);
                if (itemKeyValue < value)
                {
                    itemKeyValue = value;
                }
            }

            return itemKeyValue;
        }

        public T GetRecord(int itemKey)
        {
            var records = GetRecords();
            foreach (var record in records)
            {
                var value = GetItemKeyValue(record);
                if (value == itemKey)
                {
                    return record;
                }
            }

            return null;
        }

        public IEnumerable<T> GetRecords()
        {
            try
            {
                var content = $"{File.ReadAllText(_dBPath)}]";
                var objects = JsonConvert.DeserializeObject<List<T>>(content);

                return objects;
            }
            catch
            {
                return null;
            }
        }

        public string GetRawData()
        {
            return File.ReadAllText(_dBPath);
        }

        public DbResponse InsertRecord(T record)
        {
            try
            {
                if (RecordExists(record))
                {
                    throw new ArgumentException("Record with passed ItemKey already exists");
                }

                var serializedRecord = JsonConvert.SerializeObject(record);
                if (File.Exists(_dBPath))
                {
                    File.AppendAllText(_dBPath, $"{serializedRecord},");
                }
                else
                {
                    File.WriteAllText(_dBPath, $"[{serializedRecord},");
                }

                return new DbResponse(true);
            }
            catch (Exception ex)
            {
                return new DbResponse(false, ex);
            }
        }

        public DbResponse InsertWithNextHighestItemKey(T item)
        {
            var clone = (T)item.Clone();
            var highestItemKey = GetHighestItemKey();
            if (highestItemKey == -1)
            {
                return InsertRecord(clone);
            }
            else
            {
                AssignItemKey(clone, highestItemKey);
                InsertRecord(clone);
                return new DbResponse(true);
            }
        }

        public DbResponse InsertRecords(IEnumerable<T> records)
        {
            foreach (var record in records)
            {
                var response = InsertRecord(record);
                if (!response.IsSuccess)
                {
                    return response;
                }
            }

            return new DbResponse(true);
        }

        public DbResponse ModifyRecord(int localDbItemKey, T record)
        {
            var allObjects = GetRecords().ToList();
            try
            {
                var removed = allObjects.RemoveAll(x => (int)x.GetType().GetProperty(_itemKeyPropName).GetValue(x) == localDbItemKey);
                if (removed == 0)
                {
                    throw new NullReferenceException($"Object with ID:[{localDbItemKey}] does not exist in [{_filePath}]");
                }

                allObjects.Add(record);
            }
            catch (Exception ex)
            {
                return new DbResponse(false, ex);
            }

            var response = OverwriteAllRecords(allObjects);
            if (response.IsSuccess)
            {
                return new DbResponse(true);
            }

            return response;
        }

        public DbResponse DeleteRecord(int dbKeys)
        {
            var allObjects = GetRecords().ToList();
            try
            {
                var removed = allObjects.RemoveAll(x => (int)x.GetType().GetProperty(_itemKeyPropName).GetValue(x) == dbKeys);
                if (removed == 0)
                {
                    throw new NullReferenceException($"Object with ID:[{dbKeys}] does not exist in [{_filePath}]");
                }
            }
            catch (Exception ex)
            {
                return new DbResponse(false, ex);
            }

            var response = OverwriteAllRecords(allObjects);
            if (response.IsSuccess)
            {
                return new DbResponse(true);
            }

            return response;
        }

        public DbResponse DeleteRecords(IEnumerable<int> dbItemKeys)
        {
            foreach (var dbItemKey in dbItemKeys)
            {
                var response = DeleteRecord(dbItemKey);
                if (!response.IsSuccess)
                {
                    return response;
                }
            }

            return new DbResponse(true);
        }

        private bool RecordExists(T record)
        {
            var objects = GetRecords();
            if (objects == null)
            {
                return false;
            }

            var passedRecordValue = GetItemKeyValue(record);
            foreach (var obj in objects)
            {
                var actualRecordValue = GetItemKeyValue(obj);
                if (passedRecordValue == actualRecordValue)
                {
                    return true;
                }
            }

            return false;
        }

        private int GetItemKeyValue(T record)
        {
            var props = typeof(T).GetProperties();
            foreach (var prop in props)
            {
                if (prop.GetCustomAttribute(typeof(LocalDbItemKey)) != null)
                {
                    return (int)prop.GetValue(record);
                }
            }

            return -1;
        }

        private DbResponse OverwriteAllRecords(IEnumerable<T> records)
        {
            var serializedRecords = new StringBuilder();
            foreach (var record in records)
            {
                var serialized = JsonConvert.SerializeObject(record);
                serializedRecords.Append($"{serialized},");
            }

            try
            {
                File.WriteAllText(_dBPath, $"[{serializedRecords.ToString()}");
                return new DbResponse(true);
            }
            catch (Exception ex)
            {
                return new DbResponse(false, ex);
            }
        }

        private void SetDatabaseDirectories(string dir)
        {
            string mainDatabaseDirectory = "../../../../Database";
            if (!Directory.Exists(mainDatabaseDirectory))
            {
                Directory.CreateDirectory(mainDatabaseDirectory);
            }

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        private void SetItemKeyPropName()
        {
            var props = typeof(T).GetProperties();
            foreach (var prop in props)
            {
                if (prop.GetCustomAttribute(typeof(LocalDbItemKey)) != null)
                {
                    _itemKeyPropName = prop.Name;
                }
            }
        }

        private void AssignItemKey(T record, int newKey = -1)
        {
            var prop = record.GetType().GetProperty(_itemKeyPropName);
            var propValue = prop.GetValue(record);
            if (newKey == -1)
            {
                var newValue = int.Parse(propValue.ToString()) + 1;
                prop.SetValue(record, newValue);
            }
            else
            {
                prop.SetValue(record, newKey);
            }
        }
    }
}
